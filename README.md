# Bernardo Montoya - React

An example project in React. Live demo: https://bmontoya-marvel.herokuapp.com/ API: https://developer.marvel.com/

## Dependencies

* material-ui/core: ^4.8.3
* material-ui/icons: ^4.5.1
* react-router-dom: ^5.1.2
* react-spring: ^8.0.27
* react-window": ^1.8.5
* react-window-infinite-loader: ^1.0.5
* styled-components: ^5.0.0

## Commands

To run type:

### `npm install`
### `npm start`