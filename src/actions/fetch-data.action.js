import Configuration from '../configuration/lists.configuration';

function buildFilterString(userFilters, configFilters) {
    if(configFilters !== undefined && configFilters.length > 0) {
        let currentFilters = configFilters.map(function (configFilter, index) {
            const currentFilterConfig = configFilter.type + configFilter.condition;
            const currentFilterUser = userFilters[currentFilterConfig];
            return `${(currentFilterUser ? '-' + configFilter.condition : configFilter.condition) + (index +1 === configFilters.length ? '' : '%2C')}`}).join('')
        return currentFilters !== '' ? 'orderBy=' + currentFilters + '&' : ''
    }
    else {
        return ''
    }
}
function buildFilters(filters, resource) {
    const search = filters.search;
    const searchFilters = Configuration[resource].searchFilters;
    const searchString = (search !== '' ? searchFilters.map(function (filter, index) { return `${filter.filter}=${search + (index === searchFilters.length ? '' : '&')}`}).join('') : '');
    const filterString = buildFilterString(filters, Configuration[resource].filters);
    const queryString = searchString + filterString;
    return queryString  
}
export default function FetchData(resource, limit, offset, filters, filtering, valueOnly, value, searchFilters, configuration) {
    const apiURL = 'https://gateway.marvel.com:443/v1/public/';
    const apiKey = '5d65cb9bb14a52c7f0638109c69e8ef7';
    if (valueOnly) {
        return apiURL + resource + '/' + value + '?apikey=' + apiKey;
    } 
    else {
        let filtersString = buildFilters(filters, resource);
        console.log(filtersString);
        const url = apiURL + resource + '?' + (filtersString !== '' ? filtersString : '') + 'limit=' + limit + '&offset=' + (filtering ? 0 : offset) + '&apikey=' + apiKey;
        return url
    }
}