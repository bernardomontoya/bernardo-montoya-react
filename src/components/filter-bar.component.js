import React from 'react';
import styled from 'styled-components';
import Chip from '@material-ui/core/Chip';
import IconExpandMore from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import SearchInput from './search-input.component';
import { makeStyles, useTheme } from '@material-ui/core/styles';

// styles
const FiltersContainer = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  justify-content: row;
  align-items: center;
  flex-direction: column;
  & .marvel-filters-content {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    width: 90%;
    height: 100%;
    position: relative;
    & .marvel-list-total {
      position: absolute;
      left: 0;
      & > span {
        color: #9c9c9c;
        font-size: 16px;
        font-weight: 800;
      }
    }
    & .marvel-filters {
      display: flex;
      flex-direction: row;
      align-items: center;
      position: absolute;
      right: 0;
    }
  }
`
const FilterBar = ({userFilters, total, filterItems, config}) => {
    // Component variables
    const theme = useTheme();
    const filters = config.filters;
    const hasSearch = config.hasSearch;
    const searchFilters = config.searchFilters;
    const secondaryColor = theme.palette.secondary.main;
    const BuildFilters = filters.map((filter, i) => {
      const filterType = filter.type;
      const filterCondition = filter.condition;
      const filterDescription = filter.description;
      const filterInvert = filter.invert;
      const filterLabel = filter.label;
      const filterInvertedLabel = filter.invertedLabel;
      const filterActiveName = filterType + filterCondition;
      const filterActive = userFilters[filterActiveName];
      const StyledChip = styled(Chip)`
      margin-right: 10px !important;
      &:last-child {
        margin-right: 0 !important;
      }
      & > svg {
          transition: .2s all ease;
          transform: rotate(${ filterActive ? 180 : 0 }deg);
      }
  `
      return(
        <Tooltip key={i.toString()} title={filterDescription} placement="top-start">
          <StyledChip
            size='small'
            key={i}
            color="primary"
            clickable
            icon={<IconExpandMore />}
            label={filterActive && filterInvert ? filterInvertedLabel : filterLabel}
            onClick={()=>filterItems(filterType, filterCondition, null)}/>
        </Tooltip>
      )
    })
    return(
        <FiltersContainer>
          <div className="marvel-filters-content">
            <div className="marvel-list-total">
                <span>{total} results</span>
            </div>
            <div className="marvel-filters">
                {BuildFilters}
                {
                  hasSearch
                  ? 
                  <SearchInput label={config.searchLabel} filterItems={filterItems} searchFilters={searchFilters} />
                  :
                  null
                }      
            </div>
          </div>
        </FiltersContainer>
    )
}

export default FilterBar;