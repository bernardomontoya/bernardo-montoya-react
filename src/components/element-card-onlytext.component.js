import React from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/FavoriteOutlined';
import ShareIcon from '@material-ui/icons/Share';
import Typography from '@material-ui/core/Typography';
import Chips from './element-card.chip.component';
import Button from '@material-ui/core/Button';

import styled from 'styled-components';

export default function ElementCardOnlyText(props) { 
  // component variables
  const item = props.element;
  const config = props.config;
  const resource = config.resource;
  const chipsElements = config.chips;
  const cardWidth = config.cardWidth;
  const cardHeight = config.cardHeight;
  const cardMargin = config.cardMargin;
  const id = item.id;
  const name = item[config.titleKey];
  const theme = useTheme();
    // styles
  const StyledCard = styled(Card)`
    width: 100%;
    background: ${theme.palette.primary.main};
    position: relative;
  `
  const IconsContainer = styled.div`
    position: absolute;
    top: 12px;
    right: 12px;
    display: flex;
    flex-direction: column;
  `
  const TextOverlay = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: rgb(43,44,59);
    background: linear-gradient(240deg,rgba(43,44,59,1) 0%,rgba(255,255,255,0) 53%);
  `
  const useStyles = makeStyles({
    card: {
      display: 'flex',
      width: cardWidth + 'px',
      height: cardHeight + 'px',
      flexDirection: 'column',
      marginRight:  cardMargin + 'px',
      '&:last-child': {
        marginRight: 0
      },
    },
    cardActions: {
        justifyContent: 'space-between'
    },
    cardContent: {
        height: '100%'
    }
  })
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  return (
    <Card className={classes.card} raised={true}>
        <CardContent className={classes.cardContent}>
            <Typography variant="body2" component="p">
                {name}
            </Typography>
        </CardContent>
        <CardActions className={classes.cardActions}>
            <Button size="small">Learn More</Button>
            <div className={classes.cardDetailsContainer}>
                <Chips chipsArray={chipsElements} item={item}/>
            </div>
        </CardActions>
    </Card>
  );
}