import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/FavoriteOutlined';
import ShareIcon from '@material-ui/icons/Share';
import Typography from '@material-ui/core/Typography';
import Chips from './element-card.chip.component';
import styled from 'styled-components';

function HandleFavClick(fn, id, resource, setActive, item, config) {
  const jsonFavorites = localStorage.getItem('favorites');
  const favoriteItems = jsonFavorites === null ? [] : Array.from(JSON.parse(jsonFavorites));
  const findElement = favoriteItems.find(favorite => favorite.id === id && favorite.resource === resource);
  console.log(fn);
  fn(id, resource, item, config);
  if(findElement === undefined){
    setActive(false)
  }
  else {
    setActive(true)
  }
}

export default function ElementCard(props) { 
  // component variables
  const [active, setActive] = useState(0);
  const item = props.element;
  const config = props.config;
  const addFavorite= props.addFavorite;
  const resource = config.resource;
  const chipsElements = config.chips;
  const cardWidth = config.cardWidth;
  const cardHeight = config.cardHeight;
  const cardMargin = config.cardMargin;
  const cardTitleHeight = config.cardTitleHeight;
  const id = item.id;
  const thumbnail = item.thumbnail.path + '.' + item.thumbnail.extension;
  const name = item[config.titleKey];
  const theme = useTheme();
  const jsonFavorites = localStorage.getItem('favorites');
  const favoriteItems = jsonFavorites === null ? [] : Array.from(JSON.parse(jsonFavorites));
  const findElement = favoriteItems.find(favorite => favorite.id === id && favorite.resource === resource);
    // styles
  const StyledCard = styled(Card)`
    width: 100%;
    height: ${cardHeight}px;
    background: ${theme.palette.primary.main};
    position: relative;
  `
  const StyledCardMedia = styled(CardMedia)`
    height: ${cardHeight}px;
  `
  const IconsContainer = styled.div`
    position: absolute;
    top: 12px;
    right: 12px;
    display: flex;
    flex-direction: column;
  `
  const TextOverlay = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: rgb(43,44,59);
    background: linear-gradient(240deg,rgba(43,44,59,1) 0%,rgba(255,255,255,0) 53%);
  `
  const useStyles = makeStyles({
    cardContainer: {
      display: 'flex',
      width: cardWidth + 'px',
      minWidth: cardWidth + 'px',
      flexDirection: 'column',
      marginRight:  cardMargin + 'px',
      '&:last-child': {
        marginRight: 0
      },
    },
    cardContentContainer: {
      height: cardTitleHeight + 'px',
      display: 'flex',
      flexDirection: 'row',
      padding: '12px 0'
    },
    cardTitleContainer: {
      width: '70%',
      marginRight: '10px'
    },
    cardTitle: {
      margin: 0,
      padding: 0,
      fontSize: '15px',
      color: '#FFF',
      fontWeight: 600
    },
    cardDetailsContainer: {
      width: '30%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    }
  })
  const classes = useStyles();
  return (
    <div className={classes.cardContainer}>
      <StyledCard raised={true}>
        <CardActionArea>
          <StyledCardMedia
            image={thumbnail}
            title={name}
          />
          <TextOverlay/>
        </CardActionArea>
        <IconsContainer>
          <IconButton aria-label="add to favorites" size="small" color={findElement === undefined ? 'default' : 'secondary'} onClick={(e)=>HandleFavClick(addFavorite, id, resource, setActive, item, config)}>
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label="share" size="small">
            <ShareIcon />
          </IconButton>
        </IconsContainer>
      </StyledCard>
      <div className={classes.cardContentContainer}>
        <div className={classes.cardTitleContainer}>
          <Link to={'/' + resource +'/' + id}>
            <p className={classes.cardTitle}>{name}</p>
          </Link>
        </div>
        <div className={classes.cardDetailsContainer}>
          <Chips chipsArray={chipsElements} item={item}/>
        </div>
      </div>
    </div>
  );
}