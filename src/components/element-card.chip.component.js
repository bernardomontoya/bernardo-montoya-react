import React from 'react';
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import TimelineIcon from '@material-ui/icons/Timeline';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';

const useStyles = makeStyles({
  chip: {
    marginRight: '4px',
    backgroundColor: 'transparent',
    '&:last-child': {
      marginRight: 0
    },
    '& svg': {
      width: '16px',
      height: '16px',
      color: '#b3b3b3'
    },
    '& span': {
      fontSize: '12px',
      fontWeight: 400
    }
  }
})

export default function Chips(props) {
    const classes = useStyles();
    const chipsArray = props.chipsArray;
    const item = props.item;
    const chipItems = chipsArray.map((chip, i) =>
        <Tooltip key={i.toString()} title={chip.description} placement="top-start">
          <Chip icon={chip.value === 'comics' ? <MenuBookIcon /> : chip.value=== 'characters' ? <EmojiEmotionsIcon /> : <TimelineIcon />} className={classes.chip} label={item[chip.value].available} clickable size="small" />
        </Tooltip>
    );
    return chipItems
}