import React from 'react';
import styled from 'styled-components';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/FavoriteOutlined';
import ShareIcon from '@material-ui/icons/Share';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

// helper functions
function HandleFavClick(fn, id, resource, setActive, item, config) {
    const jsonFavorites = localStorage.getItem('favorites');
    const favoriteItems = jsonFavorites === null ? [] : Array.from(JSON.parse(jsonFavorites));
    const findElement = favoriteItems.find(favorite => favorite.id === id && favorite.resource === resource);
    console.log(fn);
    fn(id, resource, item, config);
    if(findElement === undefined){
      setActive(false)
    }
    else {
      setActive(true)
    }
  }

const CharacterContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    & .marvel-character-content-container {
        width: 90%;
        height: auto;
        position: relative;
        & .marvel-character-top-container {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin: 50px 0;
            & .marvel-character-title-container {
                margin-top: -18px;
                text-align: center;
                backdrop-filter: saturate(180%) blur(20px);
                padding: 6px 18px;
                border-radius: 4px;
                background: transparent;
                & h1 {
                    font-size: 42px;
                    color: #fff;
                }
            }
        }
        .marvel-character-description-container { 
            margin-bottom: 50px;
            & p {
                color: #9c9c9c;
                font-size: 22px;
            }
        }
    }

`

export default function Character(props) {
    const character = props.character;
    const config = props.config;
    const name = character[config.titleKey];
    const description = character.description;
    const thumbnail = character.thumbnail.path + '.' + character.thumbnail.extension;
    const comics = character.comics;
    const stories = character.stories;
    const thumbnailWidth = config.thumbnailWidth;
    const thumbnailHeight = config.thumbnailHeight;
    const addFavorite = props.addFavorite;
    // styles
    const ThumbnailContainer = styled(Paper)`
        height: ${thumbnailHeight}px;
        width: ${thumbnailWidth}px;
        background-size: cover;
        background-position: top;
        background-image: url("${thumbnail}");
    `
    const ActionButton = styled(IconButton)`
        position: absolute !important;
        & svg {
            width: 50px;
            height: 50px;
        }
        &.marver-share-character {
            left: 15%;
        }
        &.marver-favorite-character {
            right: 15%;
        }
    `
    // helper functions 
    return(
        <CharacterContainer>
            <div className="marvel-character-content-container">
                <div className="marvel-character-top-container">
                    <ThumbnailContainer elevation={1} className="marvel-character-thumbnail-container"/>
                    <Paper elevation={24} className="marvel-character-title-container">
                        <h1>{name}</h1>
                    </Paper>
                    <ActionButton className="marver-share-character" aria-label="share" size="medium">
                        <ShareIcon />
                    </ActionButton>
                    <ActionButton className="marver-favorite-character" aria-label="add to favorites" size="medium">
                        <FavoriteIcon />
                    </ActionButton>
                </div>
                { description ?
                    <div className="marvel-character-description-container">
                        <p>{description}</p>
                    </div>
                    : null
                }
            </div>
        </CharacterContainer>
    )
}