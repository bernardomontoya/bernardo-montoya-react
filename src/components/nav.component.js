import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { ReactComponent as Logo } from '../assets/svg/logo.svg';
import styled from 'styled-components';
import {useSpring, animated} from 'react-spring'
import { useTheme } from '@material-ui/core/styles';
import FavoriteIcon from '@material-ui/icons/FavoriteOutlined';
import IconButton from '@material-ui/core/IconButton';

export default function Nav(props) {
    // Function variables
    const theme = useTheme();
    const palette = theme.palette;
    const primaryColor = palette.primary.main;
    const primaryLightColor = palette.primary.light;
    const secondaryColor = palette.secondary.main;
    // Styles
    const NavContainer = styled.div`
        width: 100%;
        height: 48px;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        align-items: center;
        background: rgba(0,0,0,0.8);
        z-index: 99;
        backdrop-filter: saturate(180%) blur(20px);
        & .marv-nav-content {
            width: 90%;
            height: 100%;
            display: -webkit-flex;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            position: relative;
            & .marv-nav-logo {
                height: 100%;
                display: -webkit-flex;
                display: flex;
                flex-direction: column;
                align-items: center;
                position: absolute;
                left: 0;
                & a {
                    height: 100%;
                    display: -webkit-flex;
                    display: flex;
                    flex-direction: row;
                }
                & svg {
                    width: 120px;
                    height: auto;
                }
            }
            & .marv-nav-links {
                width: 100%;
                height: 100%;
                display: -webkit-flex;
                display: flex;
                flex-direction: row;
                justify-content: center;
                & > a {
                    color: #FFF;
                    font-size: 12px;
                    font-family: 'Montserrat', sans-serif;
                    font-weight: 700;
                    text-transform: uppercase;
                    margin-right: 50px;
                    height: 100%;
                    display: -webkit-flex;
                    display: flex;
                    flex-direction: column;
                    justify-content: center;        
                    &:last-child {
                        margin-right: 0;
                    }
                }
            }
            & .marvel-nav-favorites {
                position: absolute;
                right: 0;
                & .marvel-active-favorite-nav svg {
                    fill: ${secondaryColor}
                }
            }
        }

    `
    const ActiveIndicator = styled(animated.div)`
        width: 60px;
        height: 4px;
        background: ${secondaryColor};
        position: absolute;
        bottom: -2px; 
        border-radius: 2px;
        box-shadow: 1px 1px 6px ${secondaryColor};
    `
    const [currentLeft, setCurrentLeft] = useState(0);
    const [activeLeft, setActiveLeft] = useState(0);
    const [activeLink, setActiveLink] = useState(0);
    let activeLinkProps = useSpring({left: activeLeft, from: {left: currentLeft}, config: {mass: 1.25, tension: 250}})
    useEffect(() => {
        const activeNav = document.querySelector(".marvel-active-nav");
        const currentIndicator = document.querySelector(".marvel-nav-active-indicator");
        if (activeNav && activeLink !== "home") {
            const activeNavLeft = activeNav.getBoundingClientRect().left;
            const currentActiveNavWidth = activeNav.getBoundingClientRect().width / 2;
            const currentIndicatorLeft = currentIndicator.getBoundingClientRect().left;
            const currentIndicatorWidth = currentIndicator.getBoundingClientRect().width / 2;
            setActiveLeft((activeNavLeft + currentActiveNavWidth) - currentIndicatorWidth);
            setCurrentLeft(currentIndicatorLeft);
        }
        else {
            // remove element
        }
    }, [activeLink]);
    return(
        <NavContainer>
            <div className="marv-nav-content">
                <div className="marv-nav-logo">
                    <NavLink to="/" exact={true} onClick={() => setActiveLink("home")}><Logo/></NavLink>
                </div>
                <div className="marv-nav-links">
                    <NavLink to="/characters" activeClassName='marvel-active-nav' onClick={() => setActiveLink("characters")}>Characters</NavLink>
                    <NavLink to="/comics" activeClassName='marvel-active-nav' onClick={() => setActiveLink("comics")}>Comics</NavLink>
                    <NavLink to="/stories" activeClassName='marvel-active-nav' onClick={() => setActiveLink("stories")}>Stories</NavLink>
                </div>
                <div className="marvel-nav-favorites">
                    <NavLink to="/favorites"activeClassName='marvel-active-favorite-nav'>
                        <IconButton aria-label="favorites" >
                            <FavoriteIcon />
                        </IconButton>
                    </NavLink>
                </div>
            </div>
            <ActiveIndicator className="marvel-nav-active-indicator" style={activeLinkProps} ></ActiveIndicator>
        </NavContainer>
    )
}