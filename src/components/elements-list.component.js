import React from 'react';
import styled from 'styled-components';
import { FixedSizeList as List } from 'react-window';
import InfiniteLoader from "react-window-infinite-loader";
import ElementCard from './element-card.component';
import ElementCardOnlyText from './element-card-onlytext.component';

// Styles
const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const ElementsList = ({ items, total, hasNextPage, moreItemsLoading, loadMore, config, addFavorite}) => {
    // Global variables
    const item_margin = config.cardMargin;
    const item_width = config.cardWidth + item_margin;
    const item_height = config.cardHeight;
    const item_text_height = config.cardTitleHeight;
    const row_margin = config.rowMargin;
    const textOnly = config.textOnly;
      // Helper functions
    function getRowsAmount(width, itemsAmount) {
      const maxItemsPerRow = getMaxItemsAmountPerRow(width);
      return Math.ceil(itemsAmount/ maxItemsPerRow);
    }
    function getMaxItemsAmountPerRow(width) {
        return Math.max(Math.floor(width / (item_width + item_margin * 2)), 1);
    }
    function generateIndexesForRow(rowIndex, maxItemsPerRow, itemsAmount) {
        const result = [];
        const startIndex = rowIndex * maxItemsPerRow;
        for (let i = startIndex; i < Math.min(startIndex + maxItemsPerRow, itemsAmount); i++) {
          result.push(i);
        }
        return result;
    }
    const currentParent = document.getElementById("marvel-app-container");
    const parentWidth = currentParent.getBoundingClientRect().width;
    const parentHeight = currentParent.getBoundingClientRect().height;
    const rowCount = getRowsAmount(parentWidth, items.length, hasNextPage);
    const itemCount = hasNextPage ? rowCount + 1 : rowCount;
    const Row = React.memo(function RowItem({id, element}) {
      if (!textOnly) {
        return (
          <ElementCard item key={id} element={element} config={config} addFavorite={addFavorite} />
        );
      }
      else {
        return (
          <ElementCardOnlyText item key={id} element={element} config={config}/>
        )
      }
    });

    const rowRenderer = ({index, style}) => {
        const maxItemsPerRow = getMaxItemsAmountPerRow(parentWidth);
        const elementsIds = generateIndexesForRow(index, maxItemsPerRow, items.length);
        const currentRowElements = elementsIds.map(elementIndex => items[elementIndex]);
        return (
          <StyledRow style={style} >
            {currentRowElements.map(elementId => {
              return (
                <Row key={elementId.id} id={elementId.id} element={elementId}/>
              )
            })}
          </StyledRow>
        )
    };
    function isItemLoaded(index) {
      const maxItemsPerRow = getMaxItemsAmountPerRow(parentWidth);
      const allItemsLoaded = generateIndexesForRow(index, maxItemsPerRow, items.length).length > 0;
      return !hasNextPage || allItemsLoaded;
    }
    return(
      <InfiniteLoader
        isItemLoaded={isItemLoaded}
        itemCount={itemCount}
        loadMoreItems={loadMore}
      >
        {({ onItemsRendered, ref }) => (
          <List
              height={parentHeight - 140}
              width={parentWidth}
              itemCount={rowCount}
              itemSize={item_height + row_margin + item_text_height }
              onItemsRendered={onItemsRendered}
              ref={ref}
              className="List"
          >
            {rowRenderer}
          </List>
        )}
      </InfiniteLoader>
    )
}

export default ElementsList;