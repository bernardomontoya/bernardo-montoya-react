import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import styled from 'styled-components';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {
    useSpring,
    useSprings,
    animated,
    useTransition,
    condition
} from "react-spring";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  }),
);

const SearchContainer = styled.div`
    border-radius: 4px;
    margin-left: 20px;
`
function HandleClick(filterItems) {
  const value = document.getElementById('marvel-input-search').value;
  filterItems('search', null, value);
}
export default function SearchInput(props) {    
    const classes = useStyles();
    const label = props.label;
    const filterItems = props.filterItems;

    return(
        <SearchContainer>
            <InputBase
                className={classes.input}
                placeholder={label}
                inputProps={{ 'aria-label': 'search google maps' }}
                id='marvel-input-search'
            />
            <IconButton aria-label="search" onClick={()=>HandleClick(filterItems)}>
                <SearchIcon />
            </IconButton>
        </SearchContainer>

    )
}