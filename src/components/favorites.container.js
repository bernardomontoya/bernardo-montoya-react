import React from 'react';
import styled from 'styled-components';
import FetchData from '../actions/fetch-data.action';
import ElementCard from './element-card.component';
import ElementCardOnlyText from './element-card-onlytext.component';

const FavoritesContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 40px 0;
    .marvel-favorites-content {
        width: 90%;
        height: 100%;
        & .marvel-favorites-section {
            width: 100%;
            margin-bottom: 40px;
            &:last-child {
                margin-bottom: 0;
            }
            & .marvel-favorites-section-title {
                color: white;
                margin-bottom: 30px;
            }
            & .marvel-favorites-section-content {
                display: flex;
                flex-direction: row;
                width: 100%;
                overflow-x: auto;
            }
        }
    }
`

function RenderFavorites(favorites, addFavorite) {
    if(favorites.length > 0){
        const config = favorites[0].config;
        const readOnly = config.readOnly;
        const favoriteItems = favorites.map((favorite, i) =>
            <ElementCard item key={i} element={favorite.element} config={config} addFavorite={addFavorite} />
        );
        return favoriteItems;
    }
    else{
        return null
    }
}

export default function Favorites(props){
    const jsonFavorites = localStorage.getItem('favorites');
    const favoriteItems = jsonFavorites === null ? [] : Array.from(JSON.parse(jsonFavorites));
    const charactersItems = favoriteItems.filter(item => item.resource === 'characters');
    const comicsItems = favoriteItems.filter(item => item.resource === 'comics');
    const storiesItems = favoriteItems.filter(item => item.resource === 'stories');
    return(
        <FavoritesContainer>
            <div className='marvel-favorites-content'>
                <div className='marvel-favorites-section'>
                    <div className='marvel-favorites-section-title'>
                        <h1>Characters ({charactersItems.length})</h1>
                    </div>
                    <div className='marvel-favorites-section-content'>
                        {RenderFavorites (charactersItems, props.addFavorite)}
                    </div>
                </div>
                <div className='marvel-favorites-section'>
                    <div className='marvel-favorites-section-title'>
                        <h1>Comics ({comicsItems.length})</h1>
                    </div>
                    <div className='marvel-favorites-section-content'>
                        {RenderFavorites (comicsItems, props.addFavorite)}
                    </div>
                </div>
                <div className='marvel-favorites-section'>
                    <div className='marvel-favorites-section-title'>
                        <h1>Stories({storiesItems.length})</h1>
                    </div>
                    <div className='marvel-favorites-section-content'>
                    </div>
                </div>
            </div>
        </FavoritesContainer>

    )
}