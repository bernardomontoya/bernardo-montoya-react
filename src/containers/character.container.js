import React from 'react';
import FetchData from '../actions/fetch-data.action';
import Character from '../components/character.component';
import LoadingScreen from '../components/loading.component';

class CharacterContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            errorMessage: '',
            isLoaded: false,
            id:  this.props.match.params.id,
            character: []
        };
    }
    componentDidMount() {
        const id = this.state.id;
        const config = this.props.config;
        const resource = config.resource;
        const FetchURL = FetchData(resource, null, null, null, false, true, id);
        fetch(FetchURL)
        .then(res => res.json())
        .then(
            (result) => {
                const resultCode = result.code;
                const resultMessage = result.status;
                if (resultCode !== 404) {
                    this.setState({
                        isLoaded: true,
                        character: result.data.results[0],
                    });
                }
                else {
                    this.setState({
                        isLoaded: true,
                        error: {
                            message: resultMessage
                        }
                    });
                }
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
        
    }
    render() {
        const { character, error, isLoaded } = this.state;
        const config = this.props.config;
        const addFavorite = this.props.addFavorite;
        if (error) {
            return <div>Error: {error.message}</div>;
        }
        else if (!isLoaded) {
            return <LoadingScreen/>;
        }
        return(
            <Character character={character} config={config} addFavorite={addFavorite} />
        )
    }
}

export default CharacterContainer;