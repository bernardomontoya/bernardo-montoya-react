import React from 'react';
import ElementsList from '../components/elements-list.component';
import FetchData from '../actions/fetch-data.action';
import FilterBar from '../components/filter-bar.component';
import ListConfiguration from '../configuration/lists.configuration';
import {withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import LoadingScreen from '../components/loading.component';

class CharactersContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resource: this.props.match.params.resource,
            configuration: ListConfiguration[this.props.match.params.resource],
            error: null,
            isLoaded: false,
            items: [],
            total: 0,
            offset: 0,
            limit: 100,
            moreItemsLoading: false,
            hasNextPage: true,
            orderByName: false,
            orderbyModified: false,
            orderByIssueNumber: false,
            orderByTitle: false,
            search: ''
        };
        this.fetchItems = this.fetchItems.bind(this);
        this.loadMoreItems = this.loadMoreItems.bind(this);
        this.filterItems = this.filterItems.bind(this);
    }
    loadMoreItems() {
        const resource = this.state.resource;
        const limit = this.state.limit;
        const previousOffest = this.state.offset;
        const currentOffset = previousOffest + limit;
        const searchFilters = ListConfiguration[resource].searchFilters;
        const filters = {
            orderByname: this.state.orderByName,
            orderBymodified: this.state.orderbyModified,
            orderByissueNumber: this.state.orderByIssueNumber,
            orderBytitle: this.state.orderByTitle,
            search: this.state.search
        }
        if (currentOffset > previousOffest) {
            this.setState({ moreItemsLoading: true}, () => {
                const FetchURL = FetchData(resource, this.state.limit, currentOffset, filters, false, false, null, searchFilters, this.state.configuration);
                fetch(FetchURL)
                    .then(res => res.json())
                    .then(
                        (result) => {
                            const newItems = result.data.results;
                            this.setState({
                                items: [...this.state.items, ...newItems],
                                moreItemsLoading: false,
                                offset: result.data.offset
                            });
                        },
                        (error) => {
                            this.setState({
                                error
                            });
                        }
                    )
            });
        }
        else {
            console.log("already loaded");
        }
    }
    filterItems(filterType, filterName, filterValue) {
        // check values
        if (filterType === "orderBy") {
            if (filterName === 'name') {
                this.setState({orderByName: !this.state.orderByName, offset: 0, isLoaded: false}, () => {
                    this.fetchItems(this.state.resource);
                })
            }
            else if (filterName === 'modified') {
                this.setState({orderbyModified: !this.state.orderbyModified, offset: 0, isLoaded: false}, () => {
                    this.fetchItems(this.state.resource);
                })
            }
            else if (filterName === 'issueNumber') {
                this.setState({orderByIssueNumber: !this.state.orderByIssueNumber, offset: 0, isLoaded: false}, () => {
                    this.fetchItems(this.state.resource);
                })
            }
            else if (filterName === 'title') {
                this.setState({orderByTitle: !this.state.orderByTitle, offset: 0, isLoaded: false}, () => {
                    this.fetchItems(this.state.resource);
                })
            }
        }
        else if (filterType === 'search') {
            const currentSearch = this.state.search;
            if (currentSearch !== filterValue) {
                this.setState({search: filterValue, offset: 0, isLoaded: false}, () => {
                    this.fetchItems(this.state.resource);
                })
            }
        }
    }
    fetchItems(resource) {
        const searchFilters = ListConfiguration[resource].searchFilters;
        const currentResource = this.state.resource;
        const currentSearch = this.state.search;
        const filters = {
            orderByname: this.state.orderByName,
            orderBymodified: this.state.orderbyModified,
            orderByissueNumber: this.state.orderByIssueNumber,
            orderBytitle: this.state.orderByTitle,
            search: this.state.search
        }
        const FetchURL = FetchData(resource, this.state.limit, this.state.offset, filters, false, false, null, searchFilters, this.state.configuration);
        this.setState({ isLoaded: false, resource: resource}, () => {
            fetch(FetchURL)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.data.results,
                        total: result.data.total,
                        offset: result.data.offset,
                        previousOffset: result.data.offset,
                        count: result.data.count
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
        });
    }
    componentDidUpdate(prevProps) {
        const resource = this.props.match.params.resource;
        const prevResouce = prevProps.match.params.resource;
        if (resource !== prevResouce) {
            this.setState({
                isLoaded: false, search: '',
                orderByName: false,
                orderbyModified: false,
                orderByIssueNumber: false,
                orderByTitle: false,
            }, () => {
                this.fetchItems(resource);
            })
        }
    }
    componentDidMount() {
        const resource = this.state.resource;
        const searchFilters = ListConfiguration[resource].searchFilters;
        const filters = {
            orderByname: this.state.orderByName,
            orderBymodified: this.state.orderbyModified,
            orderByissueNumber: this.state.orderbyModified,
            orderBytitle: this.state.orderByTitle,
            search: this.state.search
        }
        const FetchURL = FetchData(resource, this.state.limit, this.state.offset, filters, false, false, null, searchFilters, this.state.configuration);
        fetch(FetchURL)
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    items: result.data.results,
                    total: result.data.total,
                    offset: result.data.offset,
                    previousOffset: result.data.offset,
                    count: result.data.count
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }
    render() {
        const { error, isLoaded, items, total, moreItemsLoading, hasNextPage, resource } = this.state;
        if (resource === "characters" || resource === "comics" || resource === "stories") {
            if (error) {
                return <div>Error: {error.message}</div>;
            }
            else if (!isLoaded) {
                return <LoadingScreen/>;
            }
            else {
                const userFilters = {
                    orderByname: this.state.orderByName,
                    orderBymodified: this.state.orderbyModified,
                    orderByissueNumber: this.state.orderByIssueNumber,
                    orderBytitle: this.state.orderByTitle,
                    search: this.state.search
                }
                return (
                    <>
                        <FilterBar userFilters={userFilters} total={total} filterItems={this.filterItems} config={ListConfiguration[resource]}/>
                        <ElementsList
                            items={items}
                            moreItemsLoading={moreItemsLoading} 
                            loadMore={this.loadMoreItems}
                            hasNextPage={hasNextPage}
                            config={ListConfiguration[resource]}
                            addFavorite={this.props.addFavorite}
                        />
                    </>
                )
            }
        }
        else {
            return null
        }
    }
}

export default CharactersContainer;