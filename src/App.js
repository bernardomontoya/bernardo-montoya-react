import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Nav from './components/nav.component';
import Home from './components/home.component';
import NotFound from './components/not-found.component';
import CharactersContainer from './containers/characters.container';
import CharacterContainer from './containers/character.container';
import styled from 'styled-components';
import { MuiThemeProvider } from '@material-ui/core/styles';  
import MarvelTheme from './marvel.theme';
import BodyBg from './assets/backgrounds/avengers.jpg';
import ListConfiguration from './configuration/details.configuration';
import Favorites from './components/favorites.container';
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.AddFavoriteItem = this.AddFavoriteItem.bind(this);
  }
  componentDidMount() {

  }
  AddFavoriteItem(id, resource, element, config, fn){
    let newFavorites = Array.from(JSON.parse(localStorage.getItem('favorites')) === null ? new Array() : JSON.parse(localStorage.getItem('favorites')));
    let selected = false;
    // remove current element if exists
    for( let i = 0; i < newFavorites.length; i++){ 
      if (newFavorites[i].id === id && newFavorites[i].resource === resource) {
        newFavorites.splice(i, 1); 
        selected = true;
      }
    }
    if(!selected) {
      const newItems = [
        {
          id: id,
          resource: resource,
          element: element,
          config: config
        }
      ]
      if (newFavorites.length === 0) {
        newFavorites = newItems;
      }
      else {
        newFavorites.push(newItems[0]);
      }
    }
    console.log(newFavorites);
    localStorage.setItem('favorites', JSON.stringify(newFavorites));
    return selected;
  }
  render() {
    const Body = styled.div`
      width: 100%;
      height: 100%;
      background: rgba(21, 22, 31, 0.98);
      z-index: 98;
      overflow-y: auto;
      & .marvel-active-favorite {
        fill: red
      }
    `
    const BodyBackground = styled.div`
      background: ${MarvelTheme.palette.marvel.bodyBackground};
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      position: relative;
      & .marvel-overlay {
        background-image: url("${BodyBg}");
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        filter: grayscale(1);
      }
    `
    return (
      <MuiThemeProvider theme={ MarvelTheme }>
        <Router>
          <BodyBackground>
            <div className="marvel-overlay"></div>
            <Nav/>
            <Body id="marvel-app-container">
              <Switch>
                  <Route path="/favorites" exact render={(props) => <Favorites addFavorite={this.AddFavoriteItem} {...props} />}/>
                  <Route path="/:resource" exact render={(props) => <CharactersContainer addFavorite={this.AddFavoriteItem} {...props} />}/>
                  <Route path="/characters/:id" render={(props) => <CharacterContainer config={ListConfiguration.characters} addFavorite={this.AddFavoriteItem} {...props} />}/>
                  <Route path="/comics/:id" render={(props) => <CharacterContainer config={ListConfiguration.comics} addFavorite={this.AddFavoriteItem} {...props} />}/>
                  <Redirect from="/" to="/characters" />
                  <Route component={NotFound}/>
              </Switch>
            </Body>
          </BodyBackground>
        </Router>
      </MuiThemeProvider>
    );
  }
}