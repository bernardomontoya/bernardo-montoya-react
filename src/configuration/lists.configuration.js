const ListConfiguration = {
    characters: {
        resource: 'characters',
        titleKey: 'name',
        hasSearch: true,
        searchLabel: 'Search for characters',
        searchFilters: [{filter: 'name'}, {filter: 'nameStartsWith'}],
        cardWidth: 300,
        cardHeight: 300,
        cardMargin: 28,
        cardTitleHeight: 80,
        rowMargin: 30,
        textOnly: false,
        detailWidth: 300,
        detailHeight: 300,
        filters: [
            {
                description: 'Order by alphabetical order',
                type: 'orderBy',
                condition: 'name',
                invert: true,
                label: 'A - Z',
                invertedLabel: 'Z - A'
            },
            {
                description: 'Order by modified date',
                type: 'orderBy',
                condition: 'modified',
                invert: false,
                label: 'Modified',
                invertedLabel: ''
            }
        ],
        chips: [
            {
                description: 'Comics',
                value: 'comics'
            },
            {
                description: 'Stories',
                value: 'stories'
            }
        ]
    },
    comics: {
        resource: 'comics',
        titleKey: 'title',
        hasSearch: true,
        searchLabel: 'Search for comics',
        searchFilters: [{filter: 'title'}, {filter: 'titleStartsWith'}],
        cardWidth: 260,
        cardHeight: 400,
        cardMargin: 45,
        rowMargin: 30,
        cardTitleHeight: 80,
        textOnly: false,
        detailWidth: 300,
        detailHeight: 300,
        filters: [
            {
                description: 'Order by issue number',
                type: 'orderBy',
                condition: 'issueNumber',
                invert: false,
                label: 'Issue',
                invertedLabel: ''
            },
            {
                description: 'Order by modified date',
                type: 'orderBy',
                condition: 'modified',
                invert: false,
                label: 'Modified',
                invertedLabel: ''
            },
            {
                description: 'Order by title',
                type: 'orderBy',
                condition: 'title',
                invert: false,
                label: 'Title',
                invertedLabel: ''
            }
        ],
        chips: [
            {
                description: 'Characters',
                value: 'characters'
            },
            {
                description: 'Stories',
                value: 'stories'
            }
        ]
    },
    stories: {
        resource: 'stories',
        titleKey: 'title',
        hasSearch: false,
        searchLabel: '',
        searchFilters: [],
        cardWidth: 300,
        cardHeight: 200,
        cardMargin: 45,
        cardTitleHeight: 0,
        rowMargin: 50,
        textOnly: true,
        detailWidth: 300,
        detailHeight: 300,
        filters: [
            {
                description: ' ID',
                type: 'orderBy',
                condition: 'id',
                invert: false,
                label: 'ID',
                invertedLabel: ''
            },
            {
                description: 'Modified date',
                type: 'orderBy',
                condition: 'modified',
                invert: false,
                label: 'Modified',
                invertedLabel: ''
            }
        ],
        chips: [
            {
                description: 'Characters',
                value: 'characters'
            },
            {
                description: 'Comics',
                value: 'comics'
            }
        ]
    }
}

export default ListConfiguration;