const DetailConfiguration = {
    characters: {
        resource: 'characters',
        titleKey: 'name',
        thumbnailWidth: 378,
        thumbnailHeight: 400,
        chips: [
            {
                description: 'Comics',
                value: 'comics'
            },
            {
                description: 'Stories',
                value: 'stories'
            }
        ]
    },
    comics: {
        resource: 'comics',
        titleKey: 'title',
        thumbnailWidth: 350,
        thumbnailHeight: 550,
        chips: [
            {
                description: 'Characters',
                value: 'characters'
            },
            {
                description: 'Stories',
                value: 'stories'
            }
        ]
    },
    stories: {
        resource: 'stories',
        titleKey: 'title',
        thumbnailWidth: 300,
        thumbnailHeight: 300,
        chips: [
            {
                description: 'Characters',
                value: 'characters'
            },
            {
                description: 'Comics',
                value: 'comics'
            }
        ]
    }
}

export default DetailConfiguration;